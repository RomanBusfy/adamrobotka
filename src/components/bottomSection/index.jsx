import { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";

import { CardItem } from "./CardItem";
import { InfoSection } from "./InfoSection";
import { fetchedData } from "./data";
import { Loader } from "../Loader";

export const BottomSection = () => {
  const [isLoading, setLoading] = useState(false);
  const [data, setData] = useState(null);

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setData(fetchedData);
      setLoading(false);
    }, 1500);
  }, []);

  if (isLoading || !data) return <Loader />;

  return (
    <Row className="align-items-center">
      <Col xs={12} md={6} style={{maxHeight: '400px', overflow:'scroll'}}>
        {data.map((item, idx) => (
          <CardItem key={idx} {...item} />
        ))}
      </Col>
      <Col xs={12} md={6}>
        <InfoSection />
      </Col>
    </Row>
  );
};
