import {
  ArrowUpCircleFill,
  LayerForward,
  Toggles,
  ArrowDown
} from "react-bootstrap-icons";
import { Col, Row, Button } from "react-bootstrap";

const BottomSection = () => {
  return (
    <Row className="align-items-center mt-5">
      <Col className="text-light">
        <div className="h5">5.95 kreditu</div>
        <div className="text-warning">0.85 kreditu/den</div>
      </Col>
      <Col>
        <div
          className="font-weight-bold h4 text-light"
          style={{ textDecoration: "underline", cursor: "pointer" }}
        >
          na 7 dni {" "}
          <ArrowDown />
        </div>
      </Col>
      <Col>
        <Button>Topovat inzerat</Button>
      </Col>
    </Row>
  );
};

export const InfoSection = () => {
  return (
    <>
      <div className="text-light">
        <ArrowUpCircleFill className="mr-2" />
        <span className="font-weight-bold">Autobazar.eu</span>
      </div>

      <h3 className="text-light">Topovanie inzeratu</h3>

      <div className="text-light mt-3">
        <LayerForward className="mr-2" />
        <span className="font-weight-bold align-middle">
          Popredne miesta
        </span>{" "}
        <span className="text-muted align-middle">vo vyhladavani</span>
      </div>
      <div className="text-light">
        <Toggles className="mr-2" />
        <span className="text-muted align-middle">Specialny stitok</span>{" "}
        <span className="font-weight-bold align-middle">TOP</span>
      </div>

      <BottomSection />
    </>
  );
};
