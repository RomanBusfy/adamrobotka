import PropTypes from "prop-types";

import { Row, Col, Image } from "react-bootstrap";

export const CardItem = ({ title, description, imageSource, price }) => {
  return (
    <Row className="my-3">
      <Col xs={4} className="px-0">
        <Image src={imageSource} fluid rounded />
      </Col>
      <Col xs={8} className="d-flex justify-content-center flex-column">
        <h6 className="text-light">{title}</h6>
        <p className="text-light mb-0">{description}</p>
        <p className="text-light mb-0">{price}</p>
      </Col>
    </Row>
  );
};

CardItem.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  imageSource: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
};
