import { Row, Col } from "react-bootstrap";

import { ShieldCheck } from "react-bootstrap-icons";

export const Header = () => {
  return (
    <Row>
      <Col xs={12} className="text-center">
        <ShieldCheck className="text-light h5" />
      </Col>
      <Col xs={12} className="text-center text-light font-weight-bold h3">
        Header
      </Col>
    </Row>
  );
};
