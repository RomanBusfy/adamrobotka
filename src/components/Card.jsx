import PropTypes from "prop-types";

import { Row, Col, Image } from "react-bootstrap";

export const Card = ({ title, description, imageSource, price }) => {
  return (
    <Row className="justify-content-center align-items-center">
      <Col xs={4} md={3} className="px-0">
        <Image
          src={imageSource}
          rounded
          fluid
        />
      </Col>
      <Col xs={8} md={5}>
        <Row>
          <Col>
            <h4 className="text-light">{title}</h4>
            <p className="text-light">
              {description}
            </p>
            <p className="text-light h5">{price}</p>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};


Card.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  imageSource: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
};
