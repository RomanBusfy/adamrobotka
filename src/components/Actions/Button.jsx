import PropTypes from "prop-types";

import { Col } from "react-bootstrap";

export const ActionButton = ({ title, icon, link, isLast }) => {
  return (
    <Col xs={3} md={2} className="text-light text-center" style={{ borderLeft: isLast ? '1px solid rgba(255, 255, 255, 0.4)' : null}}>
      {icon}
      <a className="pl-2 align-middle text-light" href={link}>
        {title}
      </a>
    </Col>
  );
};

ActionButton.propTypes = {
  title: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  icon: PropTypes.element.isRequired,
  isLast: PropTypes.bool,
};

ActionButton.defaultProps = {
  isLast: false,
};
