import { Row } from "react-bootstrap";
import { ActionButton } from "./Button";
import { actionButtonsMap } from "./data";

export const Actions = () => {
  return (
    <Row className="justify-content-center mt-4">
      {actionButtonsMap.map((item, idx) => (
        <ActionButton
          {...item}
          key={idx}
          isLast={idx === actionButtonsMap.length - 1}
        />
      ))}
    </Row>
  );
};
