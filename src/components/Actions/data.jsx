import {
  ListCheck,
  ListTask,
  PlusCircle,
  BarChart,
} from "react-bootstrap-icons";

export const actionButtonsMap = [
  {
    title: "Tlacit letak",
    link: "/",
    icon: <ListCheck />,
  },
  {
    title: "Upravit",
    link: "/",
    icon: <ListTask />,
  },
  {
    title: "Pridat dalsi",
    link: "/",
    icon: <PlusCircle />,
  },
  {
    title: "Ponuka",
    link: "/",
    icon: <BarChart />,
  },
];

