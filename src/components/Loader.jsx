import { Row, Spinner } from "react-bootstrap";

export const Loader = () => {
  return (
    <Row className="justify-content-center">
      <Spinner animation="border" variant="primary" />
    </Row>
  );
};
