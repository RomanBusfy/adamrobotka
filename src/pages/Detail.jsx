import { useEffect, useState, useCallback } from "react";
import { Container, Col, Row } from "react-bootstrap";

import { Header } from "../components/Header";
import { Card } from "../components/Card";
import { Actions } from "../components/Actions/index";
import { BottomSection } from "../components/bottomSection/index";
import { Loader } from "../components/Loader";

const fetchedData = {
  title: "BMW Rad Touring 318D Luxury Line",
  description: "Osobné vozidlá 08.07.2021 Súkromná osoba",
  imageSource:
    "https://thumbor.forbes.com/thumbor/960x0/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5d35eacaf1176b0008974b54%2F960x0.jpg%3FcropX1%3D790%26cropX2%3D5350%26cropY1%3D784%26cropY2%3D3349",
  price: "14 900€",
};

export const Detail = () => {
  const [isLoading, setLoading] = useState(false);
  const [data, setData] = useState(null);

  const fetchData = useCallback(() => {
    setLoading(true);
    setTimeout(() => {
      setData(fetchedData);
      setLoading(false);
    }, 800);
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <Container>
      {isLoading || !data ? (
        <Loader />
      ) : (
        <>
          <Header />
          <Card {...data} />
          <Actions />
          <Row className="justify-content-center my-5">
            <Col
              xs={3}
              md={1}
              style={{ borderTop: "1px solid rgba(255, 255, 255, 0.4)" }}
            />
          </Row>
          <h2 className="text-center text-light my-5">
            Zvyhodnite svoj inzerat a budte v čele
          </h2>
          <BottomSection />
        </>
      )}
    </Container>
  );
};
